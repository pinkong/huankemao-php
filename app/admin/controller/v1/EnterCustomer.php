<?php
/**
 * 非企微客户
 * User: 万奇
 * Date: 2021/11/26 19:03
 */

namespace app\admin\controller\v1;


use think\App;

class EnterCustomer extends BasicController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 匹配企微客户
     * User: 万奇
     * Date: 2022/2/18 17:28
     * @throws \think\db\exception\DbException
     */
    public function enter_customer_match(){
        param_receive(['id', 'external_user_id', 'follow_userid']);

        $result = (new \app\admin\model\EnterCustomer())->enter_customer_match($this->param);

        response(200, '操作成功');
    }

    /**
     * 匹配企微客户列表
     * User: 万奇
     * Date: 2022/2/18 17:28
     * @throws \think\db\exception\DbException
     */
    public function enter_customer_match_list(){
        param_receive(['id']);

        $result = (new \app\admin\model\EnterCustomer())->enter_customer_match_list($this->param);

        response(200, '', $result);
    }

    /**
     * 获取非企微客户画像
     * User: 万奇
     * Date: 2022/2/11 0025
     * @throws \think\db\exception\DbException
     */
    public function enter_customer_portrait(){
        param_receive(['external_user_id']);
        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->get_customer_portrait($this->param);

        response(200, '', $result);
    }

    /**
     * 非企微客户互动轨迹列表
     * User: 万奇
     * Date: 2022/2/11 0025
     * @throws \think\db\exception\DbException
     */
    public function enter_customer_track_list(){
        param_receive(['external_user_id', 'page', 'limit']);

        $customer = new \app\admin\model\WxkCustomerTrack();
        $result = $customer->get_customer_track_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 非企微客户跟进记录列表
     * User: 万奇
     * Date: 2021/3/25 0025
     * @throws \think\db\exception\DbException
     */
    public function enter_customer_follow_list(){
        param_receive(['external_user_id', 'follow_userid', 'page', 'limit']);

        $result = (new \app\admin\model\WxkCustomerFollow())->get_customer_follow_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 添加非企微客户跟进
     * User: 万奇
     * Date: 2021/3/23 0023
     * @throws \think\db\exception\DbException
     */
    public function add_enter_customer_follow(){
        param_receive(['external_user_id', 'follow_userid', 'add_follow_user', 'follow_type', 'content']);

        $this->param['not_qw'] = 1;
        (new \app\admin\model\WxkCustomerFollow())->add_customer_follow($this->param, 1);

        response(200, '操作成功');
    }

    /**
     * 修改非企微客户详情
     * User: 万奇
     * Date: 2021/7/13 16:56
     * @throws \think\db\exception\DbException
     */
    public function edit_enter_customer_info(){
        param_receive(['external_user_id']);

        (new \app\admin\model\EnterCustomer())->edit_enter_customer_info($this->param, $this->user_info['user_id']);

        response(200, '操作成功');
    }

    /**
     * 非企微客户详情
     * User: 万奇
     * Date: 2021/7/13 16:56
     * @throws \think\db\exception\DbException
     */
    public function enter_customer_info(){
        param_receive(['id']);

        $result     = (new \app\admin\model\EnterCustomer())->enter_customer_info($this->param);

        response(200, '操作成功', $result);
    }

    /**
     * 非企微客户退回公海
     * User: 万奇
     * Date: 2021/12/31 18:32
     */
    public function back_pool_enter_customer(){
        param_receive(['id']);

        (new \app\admin\model\EnterCustomer())->back_pool_enter_customer($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 非企微客户转交判断是否已拥有客户
     * User: 万奇
     * Date: 2021/12/24 18:39
     */
    public function enter_customer_shift_judge(){
        param_receive(['replace_staff', 'id']);

        $result   = (new \app\admin\model\EnterCustomer())->enter_customer_shift_judge($this->param);

        response(200, '操作成功', $result);
    }

    /**
     * 转交非企微客户
     * User: 万奇
     * Date: 2021/12/24 18:37
     */
    public function shift_enter_customer(){
        param_receive(['id', 'staff_id']);

        (new \app\admin\model\EnterCustomer())->shift_enter_customer($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 非企微客户打标签/移除标签
     * User: 万奇
     * Date: 2021/12/17 18:41
     */
    public function enter_customer_tagging(){
        param_receive(['id', 'tag_ids', 'type']);

        (new \app\admin\model\EnterCustomer())->enter_customer_tagging($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 导出非企微客户
     * User: 万奇
     * Date: 2021/12/17 18:41
     * @throws \think\db\exception\DbException
     */
    public function export_customer_import(){
        $this->param['export'] = 1;
        $result     = (new \app\admin\model\EnterCustomer())->get_enter_customer_list($this->param);

        response(200, '操作成功', $result['data']);
    }

    /**
     * 导入非企微客户
     * User: 万奇
     * Date: 2021/7/12 15:14
     */
    public function enter_customer_import(){
        $result     = (new \app\admin\model\EnterCustomer())->enter_customer_import($this->param, $this->user_info, true);

        response(200, '操作成功', $result);
    }

    /**
     * 非企微客户列表
     * User: 万奇
     * Date: 2021/12/10 17:04
     * @throws \think\db\exception\DbException
     */
    public function get_enter_customer_list(){
        param_receive(['page', 'limit']);

        $result = (new \app\admin\model\EnterCustomer())->get_enter_customer_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 添加非企微客户
     * User: 万奇
     * Date: 2021/12/10 17:04
     */
    public function add_enter_customer(){
        param_receive(['name', 'follow_add_way', 'follow_remark_mobiles', 'follow_userid']);
        $customer = new \app\admin\model\EnterCustomer();
        $customer->add_enter_customer($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 回显新增编辑非企微客户数据
     * User: 万奇
     * Date: 2021/11/26 19:03
     */
    public function show_edit_enter_customer(){
        $customer = new \app\admin\model\EnterCustomer();
        $result = $customer->show_edit_enter_customer($this->param);

        response(200, '', $result);
    }

    /**
     * 非企微客户字段管理列表
     * User: 万奇
     * Date: 2021/12/3 17:32
     */
    public function enter_customer_field_list(){

        $result = (new \app\admin\model\EnterCustomerField())->enter_customer_field_list($this->param);

        response(200, '', $result);
    }

    /**
     * 删除非企微客户字段管理
     * User: 万奇
     * Date: 2021/12/3 17:32
     */
    public function del_enter_customer_field(){
        param_receive(['id', 'del']);

        (new \app\admin\model\EnterCustomerField())->add_enter_customer_field($this->param);

        response(200, '操作成功');
    }

    /**
     * 编辑非企微客户字段管理
     * User: 万奇
     * Date: 2021/12/3 17:32
     */
    public function edit_enter_customer_field(){
        param_receive(['id', 'name']);

        (new \app\admin\model\EnterCustomerField())->add_enter_customer_field($this->param);

        response(200, '操作成功');
    }

    /**
     * 添加非企微客户字段管理
     * User: 万奇
     * Date: 2021/12/3 17:32
     */
    public function add_enter_customer_field(){
        param_receive([ 'pid', 'name']);

        (new \app\admin\model\EnterCustomerField())->add_enter_customer_field($this->param);

        response(200, '操作成功');
    }


}