﻿-- !!! 请注意该sql默认表前缀为 "hkm"，如不一致请自行更改表前缀再执行

SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE `hkm_cms_content_operating` MODIFY COLUMN `wx_customer_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业微信客户ID' AFTER `content_engine_title`;

ALTER TABLE `hkm_cms_content_operating` MODIFY COLUMN `wx_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业微信成员ID' AFTER `wx_customer_id`;

ALTER TABLE `hkm_sys_role` MODIFY COLUMN `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色id(主键)' FIRST;

ALTER TABLE `hkm_sys_role` MODIFY COLUMN `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名' AFTER `id`;

ALTER TABLE `hkm_wxk_app` MODIFY COLUMN `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用id(主键)' FIRST;

ALTER TABLE `hkm_wxk_customer` MODIFY COLUMN `follow_userid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '外部联系人的企业成员userid' AFTER `tag_ids`;

ALTER TABLE `hkm_wxk_customer` MODIFY COLUMN `follow_oper_userid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发起添加的userid，如果成员主动添加，为成员的userid；如果是客户主动添加，则为客户的外部联系人userid；如果是内部成员共享/管理员分配，则为对应的成员/管理员userid' AFTER `follow_add_way`;

ALTER TABLE `hkm_wxk_live_qr` MODIFY COLUMN `wxk_staff_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '使用该联系方式的成员userID列表，在type为1时为必填，且只能有一个' AFTER `code_type`;

SET FOREIGN_KEY_CHECKS=1;