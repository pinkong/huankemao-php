/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40101 SET FOREIGN_KEY_CHECKS = 0 */;


-- ----------------------------
-- Database `huankemao`
-- ----------------------------

-- --------------------------------------------------------

-- ----------------------------
-- Table structure for cms_develop_custom
-- ----------------------------
DROP TABLE IF EXISTS `cms_develop_custom`;
CREATE TABLE `cms_develop_custom` (
  `id` varchar(36)  NOT NULL COMMENT '主键',
  `date_year` int(11) NOT NULL COMMENT '年份',
  `type` tinyint(1) NOT NULL COMMENT '1-企业计划 2-成员计划',
  `staff_id` varchar(255) DEFAULT NULL COMMENT '成员ID',
  `year_target` int(11) NOT NULL COMMENT '年度目标',
  `one_quarter` int(11) DEFAULT '0' COMMENT '第一季度',
  `two_quarter` int(11) DEFAULT '0' COMMENT '第二季度',
  `three_quarter` int(11) DEFAULT '0' COMMENT '第三季度',
  `four_quarter` int(11) DEFAULT '0' COMMENT '第四季度',
  `one_month` int(11) DEFAULT '0' COMMENT '一月',
  `tow_month` int(11) DEFAULT '0' COMMENT '二月',
  `three_month` int(11) DEFAULT '0' COMMENT '三月',
  `four_month` int(11) DEFAULT '0' COMMENT '四月',
  `five_month` int(11) DEFAULT '0' COMMENT '五月',
  `six_month` int(11) DEFAULT '0' COMMENT '六月',
  `seven_month` int(11) DEFAULT '0' COMMENT '七月',
  `eight_month` int(11) DEFAULT '0' COMMENT '八月',
  `nine_month` int(11) DEFAULT '0' COMMENT '九月',
  `ten_month` int(11) DEFAULT '0' COMMENT '十月',
  `eleven_month` int(11) DEFAULT '0' COMMENT '十一月',
  `twelve_month` int(11) DEFAULT '0' COMMENT '十二月',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT='拓客计划';

-- ----------------------------
-- Table structure for cms_tuoke_project
-- ----------------------------
DROP TABLE IF EXISTS `cms_tuoke_project`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET FOREIGN_KEY_CHECKS = 1 */;